package test;

import java.io.File;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BaseTest {

	public static final String URL_TECHNETTS = "https://www.mercadolibre.com.ar/";
	public static final String WEB_DRIVER_PATH_CHROME = "lib\\chromedriver.exe";
	public static final String WEB_DRIVER_CHROME_DRIVER_PROPERTY ="webdriver.chrome.driver";

	public static WebDriver driver;
	public static WebDriverWait driverWait;
	public static String fileWithPath;
	
	public static void setup() {
		
		ChromeOptions ops = new ChromeOptions();
		ops.addArguments("--disable-notifications");
		System.setProperty(WEB_DRIVER_CHROME_DRIVER_PROPERTY, WEB_DRIVER_PATH_CHROME);
		driver = new ChromeDriver(ops);
		driver.navigate().to(URL_TECHNETTS);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driverWait = new WebDriverWait(driver,30);
		driver.navigate().refresh();
	
	}
}
