package test;

import modulos.Menu;
import pages.Home;
import pages.ListProduct;
import pages.PopUpUser;
import pages.Product;


public class Case01_testQuadMinds extends BaseTest{
	
	public Case01_testQuadMinds(){
		super.setup();
	}
	
	public void tEnterMenu()  {
		Menu.catCompu(driver, driverWait);
	}
	public void tEnterPopUP()  {
		PopUpUser btnTest = new PopUpUser(driver, driverWait);
		btnTest.clickBtn();
	}
	public void tListProduct()  {
		ListProduct btnTest2 = new ListProduct(driver, driverWait);
		btnTest2.clickBtn();
	}
	public void tCheckInfo()  {
		Product btnTest3 = new Product(driver, driverWait);
		btnTest3.check();
		btnTest3.checkPrice();

	}

}
