package restAssured;

import org.hamcrest.core.Is;
import org.testng.annotations.Test;
import io.restassured.response.Response;
import junit.framework.Assert;
import static io.restassured.RestAssured.*;


/**
 * Unit test for simple App.
 */
public class Ejercicio1 {

public static final String URL_TECHNETTS = "https://api.mercadolibre.com/sites/MLA/";
public static final String PRODUCT_1 ="search?q=Procesador%20AMD%20Ryzen%207%203700X%208%20núcleos%2064%20GB" ;
public static final String PRODUCT_DETAIL ="https://api.mercadolibre.com/items/";
public static final String PROD_01 ="MLA700369713";

@Test
public void test_01() {	
	String limit = "The requested limit is higher than the allowed. Maximum allowed is 50";
		Response resp = given()
				.param("limit","60")
				.when()
				.get(URL_TECHNETTS+ PRODUCT_1);
	Assert.assertEquals(limit, "The requested limit is higher than the allowed. Maximum allowed is 50");
	System.out.println(resp.asString());		
}
	
@Test
public void acceptMl() {
	get(PRODUCT_DETAIL +PROD_01).then().assertThat().body("accepts_mercadopago", Is.is(true)).log().all();	
}
@Test
public void verifCurrency() {
	get(PRODUCT_DETAIL +PROD_01).then().assertThat().body("currency_id", Is.is("ARS"));
}


@Test 
public void testBody() {
	Long time= get(URL_TECHNETTS+ PRODUCT_1).getTime();
	System.out.println("Response time: "+time);
}

}
	  

	  
	  
	  
	  

