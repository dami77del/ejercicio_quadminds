package modulos;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import pageFactory.AbstractPageObject;

public class Menu extends AbstractPageObject{

	public Menu(WebDriver driver, WebDriverWait driverWait) {
		super(driver, driverWait);
		// TODO Auto-generated constructor stub
	}

	
	
	public static void catCompu(WebDriver driver, WebDriverWait wait) {
		WebElement btnMenuCat = driver.findElement(By.xpath("//a[@class='nav-menu-categories-link']"));
		btnMenuCat.click();
		sleep();
		WebElement btnMenuMore = driver.findElement(By.xpath("//a[contains(text(),'Ver más categorías')]"));
		btnMenuMore.click();
		sleep();
		WebElement btnComPc= driver.findElement(By.xpath("//a[contains(text(),'Componentes de PC')]"));
		btnComPc.click();
		sleep();
		
	}
	
	
	
}
