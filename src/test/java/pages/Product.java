package pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import pageFactory.AbstractPageObject;


	public class Product extends AbstractPageObject {

		public Product(WebDriver driver, WebDriverWait driverWait) {
			super(driver, driverWait);
			// TODO Auto-generated constructor stub
		}
		WebDriverWait wait = new WebDriverWait(driver, 10);

		public void check( ) {
			String txt_coment = driver.findElement(By.className("ui-pdp-title")).getText();
			Assert.assertTrue(txt_coment.contains("Disco sólido interno Kingston A400 SA400S37/960G 960GB"));
			System.out.print("\"The title valid!\"");
		}
		
		public void checkPrice() {
			String txt = driver.findElement(By.className("price-tag-fraction")).getText();
			Assert.assertTrue(txt.contains("8.649"));
			System.out.print("The price is Valid");
		}
		
		
}
