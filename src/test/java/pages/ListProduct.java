package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import pageFactory.AbstractPageObject;

public class ListProduct extends AbstractPageObject {

	public ListProduct(WebDriver driver, WebDriverWait driverWait) {
		super(driver, driverWait);
		// TODO Auto-generated constructor stub
	}
		WebDriverWait wait = new WebDriverWait(driver, 10);

		By clickBtnCaba =  By.xpath("//*[@id=\"id_state\"]/dd[1]");
		
		
		By clickLastProd=  By.xpath("//*[@id=\"searchResults\"]/li[50]");

		//*[@id="searchResults"]/li[50]
			
	public void clickBtn() {
		WebElement btn= driver.findElement(clickBtnCaba);
		btn.click();	
		WebElement btn2= driver.findElement(clickLastProd);
		sleep();
		btn2.click();	
		
	}
}
