package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import pageFactory.AbstractPageObject;

public class PopUpUser extends AbstractPageObject {

	public PopUpUser(WebDriver driver, WebDriverWait driverWait) {
		super(driver, driverWait);
		// TODO Auto-generated constructor stub
	}
	WebDriverWait wait = new WebDriverWait(driver,20);
	
	public WebElement btnCancel = driver.findElement(By.xpath("//*[@id=\"menu-country\"]/div[3]/ul/li[1]"));
	
	public void clickBtn() {
		btnCancel.click();	
		}

}
