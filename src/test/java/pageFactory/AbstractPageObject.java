package pageFactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AbstractPageObject {

	
	protected WebDriver driver;
	protected WebDriverWait driverWait;


	public AbstractPageObject(WebDriver driver, WebDriverWait driverWait){
	this.driver = driver;
	this.driverWait = driverWait;
	PageFactory.initElements(driver, this);
	}


	//----------------- TIEMPO DE ESPERA -------------------//
	
	public static void sleep() {
	    try {
	        Thread.sleep(4000);
	    } catch (InterruptedException e) {
	        e.printStackTrace();
	    };
	}

	public static void sleep2() {
	    try {
	        Thread.sleep(5000);
	    } catch (InterruptedException e) {
	        e.printStackTrace();
	    };
	}
	
	
}
